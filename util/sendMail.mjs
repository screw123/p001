import nodemailer from 'nodemailer'
import _ from 'lodash'

import sendVerificationPIN_html from '../email_template/sendVerificationPIN.mjs'

const mail = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'frv6jgwjzq4qkpge@ethereal.email',
        pass: 'HrphwbPNKzMMfPv7dj'
    }
})

const sendMail = (profile, param, override) => {
    let infoDefault = {}
    switch(profile) {
        case 'admin':
            infoDefault = {
                from: 'hi@p001.com',
                subject: 'Thanks for joining P001',
                text: 'Admin testing',
                html: '<p>Admin testing</p>'
            }
            break
        case 'sendVerificationPIN':
            infoDefault = {
                from: 'hi@p001.com',
                to: param.to,
                subject: 'Thanks for joining P001',
                text: 'Admin testing',
                html: sendVerificationPIN_html(param)
            }
            break
        default:
            infoDefault = {
                from: 'hi@p001.com',
                subject: 'Thanks for joining P001',
                text: 'Admin testing',
                html: '<p>Admin testing</p>'
            }
    }
    const sendInfo = _.merge(infoDefault, override)
    mail.sendMail(sendInfo, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        console.log('https://ethereal.email, login: frv6jgwjzq4qkpge@ethereal.email, password: HrphwbPNKzMMfPv7dj')
    })
}


export default sendMail
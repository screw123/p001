## Schema design
All fields storing IDs should end with "_id"
Fields end with "_subset" is a sub document inside the main document, not really a separate collection.  It's also a snapshot, that will not update together with the main document

- [Generate Schema pic](http://nathanrandal.com/graphql-visualizer/)
- [Login as user](http://54.95.253.192/l?email=user@user.com&password=abcdefgh1)
- [Login as Admin](http://54.95.253.192/l?email=admin@admin.com&password=abcdefgh1)
- [testing page](http://54.95.253.192/graphiql)

Error types:
-----------------------
- NO_FIELDS_TO_UPDATE
- INVALID: Frontend shows `INVALID: data(key) cannot be data(value)`
- USER_ALREADY_ACTIVATED shows `User is already activated, redirecting to login...`
- SUSPENDED shows `Your login/account is not activated.  If you have already activated your account, please contact our support team.`
- NOT_AUTHORIZED `Not authorized`
- KEY_EXIST
- 
import _ from 'lodash'
import moment from 'moment'
import { isStaff, isAcctOwnerManager }  from '../auth/auth.mjs'


export const invoiceStatus = ['PENDING', 'PARTIAL', 'PAID', 'HOLD', 'CANCELLED']

//Custom schema
export const typeDef = `
    
    enum InvoiceStatus {
        PENDING
        PARTIAL
        PAID
        HOLD
        CANCELLED
    }
    
    extend type Query {
        getInvoice(where: queryWhere, limit: Int, offset: Int, orderBy: String, orderDirection: orderDirection): [Invoice]
    }
    
    extend type Mutation {
        addInvoice(salesOrder_id: String!, invoiceDate: GraphQLDate!, billingAddress_id: String!, chargeAmt: Float!): Invoice
    }
    
    type Invoice {
        _id: ID!,
        version: String,
        orderType: String,
        rentalOrder_id: RentalOrder,
        account_id: Account,
        printCount: Int,
        docEvent: [DocEvent],
        
        invoiceDate: GraphQLDate,
        
        billingAddress_id: Address,
        billingAddress: addressSnapShot,
        
        chargeAmt: Float,
        paidAmt: Float,
        status: InvoiceStatus, 
        
        createDateTime: GraphQLDateTime!,
        updateDateTime: GraphQLDateTime,
        createBy_id: User!,
        updateBy_id: User
    }
    
    type InvoiceList {
        _id: ID!,
        chargeAmt: Float,
        paidAmt: Float,
        status: InvoiceStatus, 
    }`


export const resolver = {
    Query: {
        getInvoice: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                isStaff(ctx)
                //2. field checking
                //3. add/mod fields
                ctx.spFunction['p001'].convertArgs(args)
                let [q, stripped_args] = ctx.evalParam['p001'](ctx.db['p001'].collection('Invoice'), args)
                //4. query & return
                const doc = await q.toArray()
                return doc
            } catch(e) { throw e }
        }
    },
    Mutation: {
        addInvoice: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)
                
                //make sure SO exists
                let checkSO = await ctx.db['p001'].collection('SalesOrder').find({_id: args.salesOrder_id}).limit(1).toArray()
                if (!checkSO) {
                    throw new ctx.err({message: "INVALID", data: {salesOrder_id: args.salesOrder_id}})
                }
                checkSO = checkSO[0]
                
                //make sure user have access to SO account
                isAcctOwnerManager(ctx, checkSO.account_id)
                //2. field checking
                
                //billing address id
                const billingAddressCheck = await ctx.db['p001'].collection('Address').find({_id: args.billingAddress_id, account_id: checkSO.account_id }).limit(1).toArray()
                    
                    console.log('billingAddressCheck=', billingAddressCheck)
                if (billingAddressCheck.length<1) {
                    throw new ctx.err({message: "NO_RECORD", data: {billingAddress_id: args.billingAddress_id }})
                }
                
                args['billingAddress'] = {
                    legalName: billingAddressCheck[0].legalName,
                    addressCountry: billingAddressCheck[0].addressCountry,
                    addressRegion: billingAddressCheck[0].addressRegion,
                    streetAddress: billingAddressCheck[0].streetAddress,
                    telephone: billingAddressCheck[0].telephone
                }
                
                const SOOutstandingAmt = checkSO.totalAmt - checkSO.billedAmt
                if (SOOutstandingAmt < args.chargeAmt) {
                    throw new ctx.err({message: "INVALID", data: {chargeAmt: args.chargeAmt }})
                }
                
                //3. add/mod fields
                args['version'] = "1"
                args['createBy_id'] = ctx.req.user._id
                args['updateBy_id'] = ctx.req.user._id
                args['createDateTime'] = moment().toDate()
                args['updateDateTime'] = moment().toDate()
                args['status'] = 'PENDING'
                args['printCount'] = 0
                args['docEvent'] = []
                args['paidAmt'] = 0
                
                //4. query & return
                
                const a = await ctx.db['p001'].collection('Invoice').insertOne(args);
                const e = await ctx.db['p001'].collection('DocEvent').insertOne({
                    docType: 'Invoice',
                    docEventType: 'CREATE',
                    doc_id: a.ops[0]['_id'],
                    msg: 'Invoice amount $' + args.chargeAmt,
                    user_id: ctx.req.user._id,
                    userName: ctx.req.user.firstName + ' ' + ctx.req.user.lastName,
                    createDateTime: moment().toDate()
                })
                const SOUpdate = await ctx.db['p001'].collection('SalesOrder').findOneAndUpdate(
                    {_id: args.salesOrder_id},
                    {$set: {
                        updateBy_id: ctx.req.user._id,
                        updateDateTime: moment().toDate()},
                    $inc: {
                        billedAmt: args.chargeAmt},
                    $push: {
                        invoiceList_id: a.ops[0]['_id'],
                        docEvent: e.ops[0]['_id']}
                    }
                )
                const invoiceUpdate = await ctx.db['p001'].collection('Invoice').findOneAndUpdate(
                    {_id: a.ops[0]['_id']},
                    {$push: {
                        docEvent: e.ops[0]['_id']}
                    },
                    {returnOriginal: false}
                )
                return invoiceUpdate.value
            } catch(e) { throw e }
        }
    },
    Invoice: {
        billingAddress: (obj) => { return obj.bllingAddress }
    }
}

const getUpdateField = (op, fields) => {
    switch(op) {
        case 'SET': return {$set: fields}
        case 'INC': return {$inc: fields}
        case 'UNSET': return {$unset: fields}
        default: throw new Error('updateOp does not support this Operator: ' + op)
    }
}
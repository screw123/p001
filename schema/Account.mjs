import _ from 'lodash'
import moment from 'moment'
import { isStaff, isTargetUser, isAdmin, isActiveUser, isTargetUserOrStaff }  from '../auth/auth.mjs'

export const accountTypeList = ['PERSONAL', 'BUSINESS', 'SPECIAL']

//Custom schema


export const addAccountForNewUser = async (ctx, accountType, owner_id) => {
    try {
        //1. check for access requirement
        const args = {
            accountType: accountType,
            owner_id: owner_id
        }
        const ownerCheck = await ctx.db['p001'].collection('User').findOne({_id: args.owner_id}, {projection:{isActive: 1, verificationPIN: 1}})

        if (ownerCheck.isActive || ownerCheck.verificationPIN===undefined) {
            console.error('Error: addAccountForNewUser can only be called with brand new user accounts.')
            throw 'Internal Error: addAccountForNewUser can only be called with brand new user accounts.'
        }
        //2. field checking
        //3. add/mod fields
        args['name'] = 'DEFAULT'
        args['isActive'] = false
        args['creationDateTime'] = moment().toDate()
        args['updateDateTime'] = args['creationDateTime']
        args['balance'] = 0
        args['priceList'] = 'DEFAULT'
        //4. query & return
        const a = await ctx.db['p001'].collection('Account').insertOne(args);
        //No need to update User doc as this is just a sub-function of a mutation
        return a.ops[0]
    } catch(e) { throw e }
}


export const typeDef = `
    enum accountType {
        PERSONAL
        BUSINESS
        SPECIAL
    }
    
    extend type Query {
        "get a list of user, only staff can access."
        getAccount(where: queryWhere, limit: Int, offset: Int, orderBy: String, orderDirection: orderDirection): [Account]
        
        "provide account ID, return details"
        getAccountById(_id:String!): Account
        
        "return array of accounts for logined user **possible dupe with getAccountById**"
        getMyAccount: [Account]
        
        "get user from ctx.req.user, then retrieve list info for all accounts related to user, plus getting a container count per type of container"
        getAccountListWithInfo: [Account]

    }
    
    extend type Mutation {
        addAccount(name: String!, accountType: String!, owner_id: String!): Account
        
        updateAccount(_id: String!, op: updateOp!, name: String, , manager_id: [String!], viewer_id: [String!], address_id: [String!], defaultBillingAddress_id: String, defaultShippingAddress_id: String, isActive: Boolean): Account
        
        "Only admin/su can update account owner"
        updateAccountOwner(_id:String!, owner_id: String!): Account
        
        "Only admin/su can update account type"
        updateAccountType(_id: String!, accountType: accountType!): Account
        
        updateAccountBalance(_id: String!, op: numOp!, amt: Float!): Account
    }
    
    type Account {
        _id: ID!,
        name: String,
        accountType: accountType!,
        
        "Is the Price List Code, not Price List ID"
        priceList: [PriceList],
        balance: Float!,
        
        "ID"
        owner_id: User!,
        
        "array of ID"
        manager_id: [User],
        
        "array of ID"
        viewer_id: [User],
        
        "array of ID"
        address_id: [Address],
        
        "ID"
        defaultBillingAddress_id: Address,
        
        "ID"
        defaultShippingAddress_id: Address,
        
        isActive: Boolean!,
        creationDateTime: GraphQLDateTime,
        updateDateTime: GraphQLDateTime,
        stripeCustomerObject: String,
        
        containerList: [Container]
    }`

export const resolver = {
    Query: {
        getAccount: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                isStaff(ctx)
                //2. field checking
                //3. add/mod fields
                ctx.spFunction['p001'].convertArgs(args)
                let [q, stripped_args] = ctx.evalParam['p001'](ctx.db['p001'].collection('Account'), args)
                //4. query & return
                const doc = await q.toArray()
                return doc
            } catch(e) { throw e }
        },
        getAccountById: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                //2. field checking
                ctx.spFunction['p001'].convertArgs(args)
                const acct = await ctx.db['p001'].collection('Account').findOne({_id: args._id},{_id: 1, owner_id:1, manager_id: 1, viewer_id: 1})
                if (acct==null) {
                    throw new ctx.err({message: "INVALID", data: {_id: args._id}})
                }
                isTargetUser(ctx, _.concat(acct.owner_id, acct.manager_id, acct.viewer_id))
                //3. add/mod fields
                //4. query & return
                const doc = await ctx.db['p001'].collection('Account').findOne({_id: args._id })
                return doc
            } catch(e) { throw e }
        },
        getMyAccount: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                console.log('getMyAccount called')
                isActiveUser(ctx)
                ctx.spFunction['p001'].convertArgs(ctx.req.user)
                //2. field checking
                //3. add/mod fields
                const myId = ctx.req.user._id
                //4. query & return
                const doc = await ctx.db['p001'].collection('Account').find({$or:[{owner_id: myId}, {manager_id: myId}, {viewer_id: myId} ]}).toArray()
                return doc
            } catch(e) { throw e }
        },
        getAccountListWithInfo: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                isActiveUser(ctx)
                console.log('getAccountListWithInfo, user=', ctx.req.user._id)
                const acctList = ctx.db['p001'].collection('Account').find({$or:[{owner_id: myId}, {manager_id: myId}, {viewer_id: myId} ]}).toArray()
                ctx.spFunction['p001'].convertArgs(ctx.req.user)
                //2. field checking
                //3. add/mod fields
                const myId = ctx.req.user._id
                
                //4. query & return
                
                return 1
            } catch(e) { throw e }
        }
    },
    Mutation: {
        addAccount: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)
                isTargetUserOrStaff(ctx, args.owner_id)
                //2. field checking
                const ownerCheck = await ctx.db['p001'].collection('User').findOne({_id: args.owner_id}, {projection:{isActive: 1}})
                if (ownerCheck==null) {
                    throw new ctx.err({message: "NOT_AUTHORIZED", data: {owner_id: args.owner_id}})
                }
                if (!(ownerCheck.isActive)) {
                    throw new ctx.err({message: "SUSPENDED", data: {owner_id: args.owner_id}} )
                }
                //3. add/mod fields
                args['isActive'] = true
                args['creationDateTime'] = moment().toDate()
                args['updateDateTime'] = args['creationDateTime']
                args['balance'] = 0
                args['priceList'] = 'DEFAULT'
                //4. query & return
                const a = await ctx.db['p001'].collection('Account').insertOne(args);
                //Fixme also insert owner into User doc
                return a.ops[0]
            } catch(e) { throw e }
        },
        updateAccount: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)
                isTargetUserOrStaff(ctx, args._id)
                //2. field checking
                const id = args._id
                const op = args.op
                const update_fields = _.omit(_.omit(args, '_id'), 'op')
                
                if (_.isEmpty(update_fields)) {
                    throw new ctx.err({message: "NO_FIELDS_TO_UPDATE", data: {} })
                }
                
                const allUserIds = _.union(args.manager_id, args.viewer_id)
                if (allUserIds.length > 0) {
                    const userCheck = await ctx.db['p001'].collection('User').find({_id: {$in: allUserIds}}, {projection:{_id: 1}}).toArray() //Fixme toarray gives [{_id:aaa{, {_id:bbb}], change it to real array. same as address
                    console.log(userCheck)
                    let nonExistUsers = _.difference(args.manager_id, userCheck)
                    if (nonExistUsers.length > 0) {
                        throw new ctx.err({message: "NOT_AUTHORIZED", data: {manager_id: nonExistUsers}})
                    }
                    nonExistUsers = _.difference(args.viewer_id, userCheck)
                    if (nonExistUsers.length > 0) {
                        throw new ctx.err({message: "NOT_AUTHORIZED", data: {viewer_id: nonExistUsers}})
                    }
                }
                
                const allAddressIds = _.compact(_.union([args.defaultBillingAddress_id, args.defaultShippingAddress_id], args.address_id))
                if (allAddressIds.length > 0) {
                    const addressCheck = await ctx.db['p001'].collection('Address').find({_id: {$in: allAddressIds}}, {_id: 1}).toArray()
                    const nonExistAddress = _.difference(args.address_id, addressCheck)
                    if (!addressCheck.includes(args.defaultBillingAddress_id)) {
                        throw new ctx.err({message: "NO_RECORD", data: {defaultBillingAddress_id: args.defaultBillingAddress_id}})
                    }
                    if (!addressCheck.includes(args.defaultShippingAddress_id)) {
                        throw new ctx.err({message: "NO_RECORD", data: {defaultShippingAddress_id: args.defaultShippingAddress_id}})
                    }
                    if (nonExistAddress.length > 0) {
                        throw new ctx.err({message: "NO_RECORD", data: {address_id: nonExistAddress}})
                    }
                }
                //3. add/mod fields
                update_fields['updateDateTime'] = moment().toDate()
                //4. query & return
                //Fixme also insert manager/viewer into User doc
                const doc = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: id }, getUpdateField(op, update_fields), {returnOriginal: false})
                return doc.value
            } catch(e) { throw e }
        },
        updateAccountBalance: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)
                isTargetUserOrStaff(ctx, args._id)
                //2. field checking
                let updateParam = {}
                if (args.op=="ADD") {
                    updateParam = {$inc: {balance: args.amt}}
                } else if (args.op=="SUB") {
                    updateParam = {$inc: {balance: (args.amt * -1)}}
                } else { throw new ctx.err({message: "INVALID", data: {op: args.op}}) }
                //3. add/mod fields
                //4. query & return
                let doc = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: args._id }, updateParam, {returnOriginal: false})
                doc = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: args._id }, {$set:{updateDateTime: moment().toDate()}}, {returnOriginal: false})
                return doc.value
            } catch(e) { throw e }
        },
        updateAccountOwner: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                isAdmin(ctx)
                ctx.spFunction['p001'].convertArgs(args)
                //2. field checking
                
                const userCheck = await ctx.db['p001'].collection('User').findOne({_id: args.owner_id}, {projection:{_id: 1}})
                if (userCheck==null) {
                    throw new ctx.err({message: "INVALID", data: {owner_id: args.owner_id}})
                }
                //3. add/mod fields
                //4. query & return
                //Fixme also insert owner into User doc
                const doc = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: args._id }, {$set:{owner_id: args.owner_id, updateDateTime: moment().toDate()}}, {returnOriginal: false})
                return doc.value
            } catch(e) { throw e }
        },
        updateAccountType: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                isAdmin(ctx)
                ctx.spFunction['p001'].convertArgs(args)
                //2. field checking
                //3. add/mod fields
                //4. query & return
                const doc = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: args._id }, {$set:{accountType: args.accountType, updateDateTime: moment().toDate()}}, {returnOriginal: false})
                return doc.value
            } catch(e) { throw e }
        }
    },
    Address: {
        account_id: async (obj, args, ctx, info) => {
            try {
                const a = await ctx.db['p001'].collection('Account').findOne({_id: obj.account_id})
                return a
            } catch(e) { throw e }
        }
    },
    User: {
        accountView_id: async (obj, args, ctx, info) => {
            try {
                if (obj.accountView_id==null) { return null }
                const doc = await ctx.db['p001'].collection('Account').find({_id: {$in: obj.accountView_id}}).toArray()
                return doc
            } catch(e) { throw e }
        },
        accountManage_id: async (obj, args, ctx, info) => {
            try {
                if (obj.accountManage_id==null) { return null }
                const doc = await ctx.db['p001'].collection('Account').find({_id: {$in: obj.accountManage_id}}).toArray()
                return doc
            } catch(e) { throw e }
        },
        accountOwn_id: async (obj, args, ctx, info) => {
            try {
                console.log('Account.User.accountOwn_id resolver', obj.accountOwn_id)
                if (obj.accountOwn_id==null) { return null }
                const doc = await ctx.db['p001'].collection('Account').find({_id: {$in: obj.accountOwn_id}}).toArray()
                return doc
            } catch(e) { throw e }
        }
    },
    Container: {
        accountOwner_id: async (obj, args, ctx, info) => {
            try {
                const doc = await ctx.db['p001'].collection('Account').findOne({_id: obj.accountOwner_id})
                return doc
            } catch(e) { throw e }
        },
    },
    Quotation: {
        account_id: async (obj, args, ctx, info) => {
            try {
                const doc = await ctx.db['p001'].collection('Account').findOne({_id: obj.account_id})
                return doc
            } catch(e) { throw e }
        },
    },
}

export const typeDefPublic = `
    type Account {
        _id: ID,
    }`
    
export const resolverPublic = {
    Quotation: {
        account_id: (obj, args, ctx, info) => { return '' },
    },
}


const getUpdateField = (op, fields) => {
    switch(op) {
        case 'SET': return {$set: fields}
        case 'INC': return {$inc: fields}
        case 'UNSET': return {$unset: fields}
        default: throw new Error('updateOp does not support this Operator: ' + op)
    }
}


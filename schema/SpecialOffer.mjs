import _ from 'lodash'
import { isAdmin } from '../auth/auth.mjs'

//Fixme come back and revisit this later on
export const typeDef = `
    
    enum SpecialOfferType {
        RENT
        SHIP_IN
        SHIP_OUT
        SHIP_OUT_AND_IN
    }

    
    extend type Query {
        getSpecialOfferMaster(where: queryWhere, limit: Int, offset: Int, orderBy: String, orderDirection: orderDirection): [SpecialOfferMaster]
    }
    
    extend type Mutation {
        "need admin"
        addSpecialOfferMaster(shortCode: String!, name: String!, longDesc: String!, durationDaysFrom: Int!, durationDaysTo: Int!, durationMonthsFrom: Int!, durationMonthsTo: Int!, containerType: [String!]!, discountFixedAmt: Float!, discountPercentage: Float!, customerType: [String!]!, minOrderQty: Int!, usePerAccount: Int!, usePerUser: Int!, validFrom: GraphQLDate!, validTo: GraphQLDate!): SpecialOfferMaster
        
        "need admin"
        updateSpecialOfferMaster(_id: String!, op: updateOp!, shortCode: String, name: String, longDesc: String, durationDaysFrom: Int, durationDaysTo: Int, durationMonthsFrom: Int, durationMonthsTo: Int, containerType: [String!], discountFixedAmt: Float, discountPercentage: Float, customerType: [String!], minOrderQty: Int, usePerAccount: Int, usePerUser: Int, validFrom: GraphQLDate, validTo: GraphQLDate, isActive: Boolean): SpecialOfferMaster
    }
    
    type SpecialOfferMaster {
        _id: ID!,
        
        specialOfferType: SpecialOfferType,
        
        "A human readable code for specialOffer, use for entering to couponCode if required"
        shortCode: String,
        
        "Long name/description"
        name: String,
        
        "Long description in markdown format"
        longDesc: String,
        
        durationDaysFrom: Int,
        durationDaysTo: Int,
        durationMonthsFrom: Int,
        durationMonthsTo: Int,
        containerType: [String],
        discountFixedAmt: Float,
        discountPercent: Float,
        customerType: [String],
        minOrderQty: Int,
        
        validFrom: GraphQLDate,
        validTo: GraphQLDate,
        isActive: Boolean!
    }`
    
export const resolver = {
    Query: {
        getSpecialOfferMaster: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                //2. field checking
                //3. add/mod fields
                ctx.spFunction['p001'].convertArgs(args)
                let [q, stripped_args] = ctx.evalParam['p001'](ctx.db['p001'].collection('SpecialOffer'), args)
                //4. query & return
                const doc =  await q.toArray()
                return doc
            } catch(e) { throw e }
        },
    },
    Mutation: {
        addSpecialOfferMaster: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                isAdmin(ctx)
                //2. field checking
                //3. add/mod fields
                args['isActive'] = true
                //4. query & return
                const a = await ctx.db['p001'].collection('SpecialOffer').insertOne(args);
                return a.ops[0]
            } catch(e) { throw e }
        },
        updateSpecialOfferMaster: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                isAdmin(ctx)
                //2. field checking
                //3. add/mod fields
                ctx.spFunction['p001'].convertArgs(args)
                const id = args._id
                const op= args.op
                let update_fields = _.omit(_.omit(args, '_id'), 'op')
                //4. query & return
                const doc = await ctx.db['p001'].collection('SpecialOffer').findOneAndUpdate({_id: id }, getUpdateField(op, update_fields), {returnOriginal: false})
                return doc.value
            } catch(e) { throw e }
        }
    }
}

const getUpdateField = (op, fields) => {
    switch(op) {
        case 'SET': return {$set: fields}
        case 'INC': return {$inc: fields}
        case 'UNSET': return {$unset: fields}
        default: throw new Error('updateOp does not support this Operator: ' + op)
    }
}
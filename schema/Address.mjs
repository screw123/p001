import _ from 'lodash'
import moment from 'moment'
import ver from 'validator'
import { isAcctOwnerManager, isActiveUser, isTargetUserOrStaff, isAdminOrSU }  from '../auth/auth.mjs'

export const addressType = ['WHS', 'CUSTOMER']

export const typeDef = `
    
    enum addressType {
        WHS
        CUSTOMER
    }
    
    extend type Query {
        "Use by staff (Admin/SU)"
        getAddress(where: queryWhere, limit: Int, offset: Int, orderBy: String, orderDirection: orderDirection): [Address]
        
        "Same as getAddress but can be used by all active users.  Will auto fill/override account_id with active logined user"
        getAddressByUser(where: queryWhere, limit: Int, offset: Int, orderBy: String, orderDirection: orderDirection): [Address]
    }
    
    extend type Mutation {
        addAddress(legalName: String!, addressCountry: String!, addressRegion1: String, streetAddress: String!, telephone: String!, account_id: String!, addressRegion2: String!, addressType: String!): Address
        
        updateAddress(_id: String!, legalName: String, addressCountry: String, addressRegion1: String, addressRegion2: String, streetAddress: String, telephone: String, isActive: Boolean): Address
    }
    
    type Address {
        _id: ID!,
        addressType: String,
        legalName: String,
        addressCountry: String!,
        addressRegion1: String,
        addressRegion2: String!,
        streetAddress: String!,
        telephone: String,
        isActive: Boolean!,
        
        "an ID of an Account that owns this Address"
        account_id: Account!,
        
        creationDateTime: GraphQLDateTime,
        updateDateTime: GraphQLDateTime
    }
    
    type addressSnapShot {
        legalName: String,
        addressCountry: String,
        addressRegion1: String,
        addressRegion2: String,
        streetAddress: String,
        telephone: String
    }
    `
    
//1. check for access requirement
//2. field checking
//3. add/mod fields
//4. query & return

export const resolver = {
    Query: {
        getAddress: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)
                isAdminOrSU(ctx)
                //2. field checking
                //3. add/mod fields
                let [q, stripped_args] = ctx.evalParam['p001'](ctx.db['p001'].collection('Address'), args)
                const doc =  await q.toArray()
                //4. query & return
                return doc
            } catch(e) { throw e }
        },
        getAddressByUser: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)
                isActiveUser(ctx)
                //2. field checking
                //3. add/mod fields
                args['account_id'] = ctx.req.user._id
                let [q, stripped_args] = ctx.evalParam['p001'](ctx.db['p001'].collection('Address'), args)
                const doc =  await q.toArray()
                //4. query & return
                return doc
            } catch(e) { throw e }
        }
    },
    Mutation: {
        addAddress: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                //2. field checking
                console.log('addAddress')
                ctx.spFunction['p001'].convertArgs(args)
                const acctCheck = await ctx.db['p001'].collection('Account').findOne({_id: args.account_id}, {projection:{_id: 1, owner_id: 1, manager_id: 1}})
                if (!acctCheck) {
                    throw new ctx.err({message: "INVALID", data: {account_id: args.account_id}})
                }
                
                isTargetUserOrStaff(ctx, _.compact(_.union([acctCheck.owner_id], acctCheck.manager_id)))
                
                if (args['legalName'].length < 1) { throw new ctx.err({message: "INVALID", data: {legalName: args.legalName}}) }
                if (args['addressCountry'].length < 1) { throw new ctx.err({message: "INVALID", data: {addressCountry: args.addressCountry}}) }
                if ((args['streetAddress'].length < 5) || (args['streetAddress'].length > 500)) { throw new ctx.err({message: "INVALID", data: {streetAddress: args.streetAddress}}) }
                if (args['addressRegion2'].length < 1) { throw new ctx.err({message: "INVALID", data: {addressRegion2: args.addressRegion2}}) }
                if (args['telephone'].length < 8) { throw new ctx.err({message: "INVALID", data: {telephone: args.telephone}}) }
                
                //3. add/mod fields
                args['isActive'] = true
                args['addressType'] = 'CUSTOMER' //not taking input from user, and hardcode to CUSTOMER
                args['creationDateTime'] = moment().toDate()
                args['updateDateTime'] = args['creationDateTime']
                //4. query & return
                const a = await ctx.db['p001'].collection('Address').insertOne(args);
                console.log('addAddress, address added=', a.ops[0])
                const b = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: args.account_id}, {$push: {address_id: a.ops[0]._id }})
                console.log('addAddress, Account updated=', b)
                return a.ops[0]
            } catch(e) { throw e }
        },
        updateAddress: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)

                //2. field checking
                const id = args._id
                const update_fields = _.omit(args, '_id')
                if (_.isEmpty(update_fields)) {
                    throw new ctx.err({message: "NO_FIELDS_TO_UPDATE", data: {} })
                }
                
                //check if user has access right to update the addres
                const address = await ctx.db['p001'].collection('Address').findOne({_id: id}, {projection: {account_id: 1, isActive: 1}} )
                console.log('updateAddress, address=',address, 'args=', args)
                isAcctOwnerManager(ctx, address.account_id)
                
                if (args['legalName'] && (args['legalName'].length < 1)) { throw new ctx.err({message: "INVALID", data: {legalName: args.legalName}}) }
                
                if (args['addressCountry'] && (args['addressCountry'].length < 1)) { throw new ctx.err({message: "INVALID", data: {addressCountry: args.addressCountry}}) }
                
                if (args['streetAddress'] && ((args['streetAddress'].length < 5) || (args['streetAddress'].length > 500)) ) { 
                    throw new ctx.err({message: "INVALID", data: {streetAddress: args.streetAddress}})
                }
                
                //if (args['addressRegion1'] && (args['addressRegion1'].length < 1)) { throw new ctx.err({message: "INVALID", data: {addressRegion2: args.addressRegion2}}) }
                
                if (args['addressRegion2'] && (args['addressRegion2'].length < 1)) { throw new ctx.err({message: "INVALID", data: {addressRegion2: args.addressRegion2}}) }
                
                if (args['telephone'] && (args['telephone'].length != 8)) { throw new ctx.err({message: "INVALID", data: {telephone: args.telephone}}) }
                
                
                
                //3. add/mod fields
                update_fields['updateDateTime'] = moment().toDate()
                //4. query & return
                
                if (args['isActive']==false && address.isActive==true) { //means user is disabling it
                    console.log('disabling address', id)
                    const account = await ctx.db['p001'].collection('Account').findOne({_id: address.account_id}, {projection: {address_id: 1, defaultBillingAddress_id: 1, defaultShippingAddress_id: 1}} )
                    
                    if (account.address_id.length<=1) { throw new ctx.err({message: "CANNOT_DISABLE_LAST_ONE", data: {address: 1}}) }  //last one cannot be disabled, throw error.  This is special key is not 'isActive' but change to 'address', so front-end can properly show err message.
                    
                    const address_id = account.address_id.filter((v)=> !v.equals(id) )
                    
                    const defaultBillingId = (id.equals(account.defaultBillingAddress_id)) ? address_id[0] : account.defaultBillingAddress_id
                    const defaultShippingId = (id.equals(account.defaultShippingAddress_id)) ? address_id[0] : account.defaultShippingAddress_id
                    
                    const a = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: account._id }, {$set: {address_id: address_id, defaultShippingAddress_id: defaultShippingId, defaultBillingAddress_id: defaultBillingId }}, {returnOriginal: false})
                }
                
                if (args['isActive']==true && address.isActive==false) { //means user is enabling it  //this part is not tested
                    console.log('enabling address, ', id)
                    const account = await ctx.db['p001'].collection('Account').findOne({_id: address.account_id}, {projection: {address_id: 1}} )
                    const address_id = account.address_id.concat([id])
                    
                    const a = await ctx.db['p001'].collection('Account').findOneAndUpdate({_id: account._id }, {$set: {address_id: address_id }}, {returnOriginal: false})
                }
                
                const doc = await ctx.db['p001'].collection('Address').findOneAndUpdate({_id: id }, getUpdateField('SET', update_fields), {returnOriginal: false})
                
                return doc.value
            } catch(e) { throw e }
        }
    },
    Account: {
        address_id: async (obj, args, ctx, info) => {
            try {
                if (obj.address_id===undefined) { return undefined }
                let addressArray = []
                for (let i=0;i<obj.address_id.length;i++) {
                    addressArray.push(ctx.spFunction['p001'].getIdObj(obj.address_id[i]))
                }
                const docs = await ctx.db['p001'].collection('Address').find({_id: {$in: addressArray}}).toArray()
                return docs
            } catch(e) { throw e }
        },
        defaultShippingAddress_id: async (obj, args, ctx, info) => {
            try {
                if (obj.defaultShippingAddress_id===undefined) { return undefined }
                const a = ctx.spFunction['p001'].convertArgs({_id: obj.defaultShippingAddress_id})
                const doc = await ctx.db['p001'].collection('Address').findOne(a)
                return doc
            } catch(e) { throw e }
        },
        defaultBillingAddress_id: async (obj, args, ctx, info) => {
            try {
                if (obj.defaultBillingAddress_id===undefined) { return undefined }
                const a = ctx.spFunction['p001'].convertArgs({_id: obj.defaultBillingAddress_id})
                const doc = await ctx.db['p001'].collection('Address').findOne(a)
                return doc
            } catch(e) { throw e }
        }
    }
}

const getUpdateField = (op, fields) => {
    switch(op) {
        case 'SET': return {$set: fields}
        case 'INC': return {$inc: fields}
        case 'UNSET': return {$unset: fields}
        default: throw new Error('updateOp does not support this Operator: ' + op)
    }
}
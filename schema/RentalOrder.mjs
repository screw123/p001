import _ from 'lodash'
import moment from 'moment'
import { isStaff, isTargetUser, isAdmin, isActiveUser, isAcctOwnerManager }  from '../auth/auth.mjs'


//Custom schema

/*
Flow must be create SO > create Invoice > create/re-use containers > complete
Status:
INIT - just created SO
BILLED - Created Invoice
PROCESSING - Credit deducted, pending to send empty boxes
PARTIAL_DELIVERED - Some boxes sent but not all
COMPLETED - All credit deducted and all boxes sent
*/

export const typeDef = `
    
    enum rentalOrderStatus {
        INIT
        BILLED
        PROCESSING
        COMPLETED
        HOLD
        CANCELLED
    }
    
    extend type Query {
        getRentalOrder(where: queryWhere, limit: Int, offset: Int, orderBy: String, orderDirection: orderDirection): [RentalOrder]
    }
    
    extend type Mutation {
        addRentalOrder(quotation_id: String!, billingAddress_id: String!, account_id: String, shippingAddress_id: String): RentalOrder
    }
    
    type RentalOrder {
        _id: ID!,
        version: String,
        printCount: Int,
        docEvent: [DocEvent],
        status: rentalOrderStatus,
        
        invoiceList_id: [InvoiceList],
        quotation_id: Quotation,
        
        billingAddress_id: Address,
        billingAddress: addressSnapShot,
        
        shippingAddress_id: Address,
        shippingAddress: addressSnapShot,
        
        docLines: [RentalOrderDetails],
        
        account_id: Account,
        accountType: String,
        
        totalAmt: Float!,
        billedAmt: Float!,
        paidAmt: Float!,
        
        createDateTime: GraphQLDateTime!,
        updateDateTime: GraphQLDateTime,
        createBy_id: User!,
        updateBy_id: User
    }
    
    type RentalOrderDetails{
        SKU_id: String!,
        SKUName: String!,
        containerList: [Container_subset],
        
        rentMode: rentMode!,
        "this is not used to calc line total price.  only to replicate to container and calc expiry date"
        duration: Int!,
        qty: Int!,
        
        rent_unitPrice: Float!,
        
        "qty x duration x unitPrice"
        rent_originalLineTotal: Float!,
        rent_unitDiscounted: Float!,
        
        "qty x duration x unitDiscounted"
        rent_discountedLineTotal: Float!,
        remarks: String
    }
    `
    

export const resolver = {
    Query: {
        getRentalOrder: async (obj, args, ctx, info) => {
            try {
                //1. check for access requirement
                //isStaff(ctx)
                //2. field checking
                //3. add/mod fields
                ctx.spFunction['p001'].convertArgs(args)
                let [q, stripped_args] = ctx.evalParam['p001'](ctx.db['p001'].collection('RentalOrder'), args)
                //4. query & return
                const doc = await q.toArray()
                return doc
            } catch(e) { throw e }
        }
    },
    Mutation: {
        addRentalOrder: async (obj, args, ctx, info) => {
            console.log('called addRentalOrder')
            try {
                /*
                1. Check validity of quotation
                2. check validity of quotation's account, and current user is owner/manager of account
                3. re-calc the quotation
                4a. create SO and status='INIT'
                4b. update quotation status
                4c. create invoice
                
                */
                //1. check for access requirement
                ctx.spFunction['p001'].convertArgs(args)
                
                let quotation = await ctx.db['p001'].collection('Quotation').find({_id: args.quotation_id}).limit(1).toArray()
                
                if (quotation.length===0) {
                    throw new ctx.err({message: "INVALID", data: {quotation_id: args.quotation_id}})
                }
                else { quotation = quotation[0] }
                
                if (quotation.status !== 'INIT') {
                    throw new ctx.err({message: "WRONG_STATUS", data: {quotation_id: args.quotation_id}})
                }
                
                
                //2. field checking
                
                let account = {}
                //check acct_id embed in quotation
                if (quotation.account_id != undefined) {
                    isAcctOwnerManager(ctx, quotation.account_id) //First check if user own/manage acct
                    account = await ctx.db['p001'].collection('Account').find({_id: quotation.account_id, isActive: true}, {accountType: 1} ).toArray() //Make sure account is valid and active
                    console.log('addRentalOrder, account=', account)
                    if (account.length<1) {
                        throw new ctx.err({message: "INVALID", data: {account_id: quotation.account_id}})
                    }
                    else { account = account[0] }
                }
                //check acct_id if supplied from query
                
                if (args.account_id != undefined) {
                    isAcctOwnerManager(ctx, args.account_id) //First check if user own/manage acct
                    account = await ctx.db['p001'].collection('Account').find({_id: args.account_id, isActive: true}, {accountType: 1} ).limit(1).toArray()//Make sure account is valid and active
                    console.log('addRentalOrder, accountNum=', account)
                    if (account.length<1) {
                        throw new ctx.err({message: "NOT_FOUND", data: {account_id: args.account_id}})
                    }
                    else { account = account[0] }
                }
                //check equality if both sides supplied
                if ((args.account_id != undefined) & (quotation.account_id != undefined)) {
                    if (!(quotation.account_id.equals(args.account_id) )) {
                        throw new ctx.err({message: "INVALID", data: {account_id: args.account_id}})
                    }
                }
                //throw error if both sides not supplied
                if ((args.account_id == undefined) & (quotation.account_id == undefined)) {
                    throw new ctx.err({message: "INVALID", data: {account_id: null}})
                }
                
                //check addresses
                const addresses_id = [args.billingAddress_id]
                if (args.shippingAddress_id) { addresses_id.push(args.shippingAddress_id) }
                const addresses = await ctx.db['p001'].collection('Address').find({_id: {$in: addresses}, isActive: true} ).toArray()
                const billingAddress = addresses.find(v=>v.equals(args.billingAddress_id))
                if ( billingAddress==undefined ) {
                    throw new ctx.err({message: "INVALID", data: {billingAddress_id: args.billingAddress_id}})
                }
                let shippingAddress
                if ( args.shippingAddress_id ) ) {
                    shippingAddress = addresses.find(v=>v.equals(args.shippingAddress_id))
                    if ( shippingAddress==undefined ) {
                        throw new ctx.err({message: "INVALID", data: {shippingAddress_id: args.shippingAddress_id}})
                    }
                }
                
                
                //3. recalc quotation
                //Trust the quotation now.  Later on should re-calc
                
                let orderLines = []
                let totalAmt = 0
                const SKU = await ctx.db['p001'].collection('SKUMaster').find({_id: {$in: quotation.quotationDetails.map(v=>v.SKU_id)}}, {_id: 1, shortCode: 1, name:1, isActive: 1}).toArray()
                
                for (let i=0;i<quotation.quotationDetails.length;i++) {
                    let discountedTotal = quotation.quotationDetails[i].unitDiscounted * quotation.quotationDetails[i].qty
                    
                    orderLines.push({
                        SKU_id: quotation.quotationDetails[i].SKU_id,
                        SKUName: SKU.find(v=> v._id==quotation.quotationDetails[i].SKU_id ),
                        rentMode: quotation.quotationDetails[i].rentMode,
                        duration: quotation.quotationDetails[i].duration,
                        qty: quotation.quotationDetails[i].qty,
                        
                        rent_unitPrice: quotation.quotationDetails[i].rent_unitPrice,
                        rent_originalLineTotal: (quotation.quotationDetails[i].rent_unitPrice * quotation.quotationDetails[i].qty),
                        
                        rent_unitDiscounted: quotation.quotationDetails[i].rent_unitDiscounted,
                        rent_discountedLineTotal: discountedTotal
                    })
                    totalAmt = totalAmt + discountedTotal
                }
                
                let rentalOrder = {
                    version: "1.0",
                    printCount: 0,
                    docEvent: [],
                    status: "INIT",
                    invoiceList_id: [],
                    containerList: [],
                    quotation_id: quotation._id,
                    billingAddress_id: args.billingAddress_id,
                    billingAddress: _.pick(billingAddress, ['legalName', 'addressCountry', 'addressRegion1','addressRegion2', 'streetAddress', 'telephone']),
                    shippingAddress_id: args.shippingAddress_id,
                    shippingAddress: (args.shippingAddress_id===undefined) ? (undefined: _.pick(shippingAddress, ['legalName', 'addressCountry', 'addressRegion1','addressRegion2', 'streetAddress', 'telephone']) ),
                    docLines: orderLines,
                    account_id: quotation.account_id || args.account_id,
                    accountType: account.accountType,
                    totalAmt: totalAmt,
                    billedAmt: 0,
                    paidAmt: 0,
                    createDateTime: moment().toDate(),
                    createBy_id: ctx.req.user._id
                }
                //insert Rental Order
                //insert invoice
                //insert update quotation status
                //create docEvent
                
                
                /* these will be used only when SO is created from scratch.  This method now create from quotation_id, so temp comment out.
                //check for qty<1 error + compile item List to get info from DB
                let SKUList = []
                for(let i=0 ; i< args.docLines.length ; i++){
                    let li = args.docLines[i]
                    
                    ctx.spFunction['p001'].convertArgs(li)
                    if (li.qty<1) { 
                        throw new ctx.err({message: "INVALID", data: {docLines: li }})
                    }
                    SKUList.push(li.SKU_id)
                }
                
                //quotation_id
                if (args.hasOwnProperty('quotation_id')) {
                    const quotationCheck = await ctx.db['p001'].collection('Quotation').find({_id: args.quotation_id, status: {$in: ['INIT', 'CONVERTED_SO']} }).limit(1).count(true)
                    
                    if (quotationCheck!=1) {
                        throw new ctx.err({message: "NO_RECORD", data: {quotation_id: args.quotation_id }})
                    }
                }
                //billing address id
                const billingAddressCheck = await ctx.db['p001'].collection('Address').find({_id: args.billingAddress_id, account_id: args.account_id }).limit(1).toArray()
                    
                if (billingAddressCheck.length<1) {
                    throw new ctx.err({message: "NO_RECORD", data: {billingAddress_id: args.billingAddress_id }})
                }
                
                args['billingAddress'] = {
                    legalName: billingAddressCheck[0].legalName,
                    addressCountry: billingAddressCheck[0].addressCountry,
                    addressRegion: billingAddressCheck[0].addressRegion,
                    streetAddress: billingAddressCheck[0].streetAddress,
                    telephone: billingAddressCheck[0].telephone
                }
                
                //3. add/mod fields
                args['version'] = "1"
                args['salesOrderType'] = 'RENTAL'
                args['createBy_id'] = ctx.req.user._id
                args['updateBy_id'] = ctx.req.user._id
                args['createDateTime'] = moment().toDate()
                args['updateDateTime'] = moment().toDate()
                args['status'] = 'INIT'
                args['accountType'] = checkAccount.accountType
                args['printCount'] = 0
                args['docEvent'] = []
                
                //calculate totalAmt + popular docLines
                args['totalAmt'] = 0
                args['billedAmt'] = 0
                args['paidAmt'] = 0
                
                //get info from DB
                const checkSKUList = await ctx.db['p001'].collection('SKUMaster').find({_id: {$in: SKUList}, isActive: true },{projection: {name:1}} ).limit(1).toArray()
                const checkPriceList = await ctx.db['p001'].collection('PriceList').find({
                    code: checkAccount.priceList,
                    rentMode: args.rentMode,
                    validFrom: { $lte: moment().toDate() },
                    validTo: { $gte: moment().toDate() },
                    item_id: {$in: SKUList}
                }).toArray()
                
                for(let i=0 ; i< args.docLines.length ; i++){
                    let li = args.docLines[i]
                    if (!_.has(li,'unitTotal')) {
                        let priceListDoc = checkPriceList.find((v)=> {
                            return (v.item_id.equals(li.SKU_id))
                        })
                        if (!priceListDoc) {
                            throw new ctx.err({message: "NO_RECORD", data: {docLines: [li] }})
                        }
                        li['unitTotal'] = priceListDoc.rent
                    }
                    li['lineTotal'] = li.unitTotal * li.qty
                    args.totalAmt = args.totalAmt + li.lineTotal
                }
                
                //4. query & return
                const a = await ctx.db['p001'].collection('SalesOrder').insertOne(args);
                return a.ops[0]*/
            } catch(e) { throw e }
        }
    },
    RentalOrder: {
        docLines: (obj) => { return obj.docLines },
        containerList: (obj) => { return obj.containerList },
    },
    Container: {
        rentalOrder_id: async (obj, args, ctx, info) => {
            try {
                const doc = await ctx.db['p001'].collection('RentalOrder').findOne({_id: obj.rentalOrder_id})
                return doc
            } catch(e) { throw e }
        }
    }
}

const getUpdateField = (op, fields) => {
    switch(op) {
        case 'SET': return {$set: fields}
        case 'INC': return {$inc: fields}
        case 'UNSET': return {$unset: fields}
        default: throw new Error('updateOp does not support this Operator: ' + op)
    }
}